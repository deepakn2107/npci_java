package practicePrograms;

interface vehicle {
	
	default void ride() {
		System.out.println("I am riding");
	}
	
	default void display() {
		System.out.println("Hello from vehicle");
	}
	
}
 
class car implements vehicle{
	public void ride() {
		System.out.println("You are riding car");
	}
	
}

class bike implements vehicle{
	public void ride()
{
		System.out.println("You are riding bike");
		
}}

class mechanic{
	void checking(vehicle v) {
		System.out.println("checking");
		v.ride();
		
	}
}

public class InterfacePractice {
	
	public static void main(String [] args) {
		
		vehicle n= new bike();
		mechanic m= new mechanic();
		car c =new car();
		bike b= new bike();
		
		
		
		m.checking(c);
		m.checking(b );
		n.display();
		n.ride();
		
		
	}

	
}
