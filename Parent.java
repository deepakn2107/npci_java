package opps;
class A{}

class B extends A{}

public class Parent {
	//overridden method
	public A m1() 
	{
		System.out.println("I am m1 method of parent");
		return new A();
	}
}
class Child extends Parent{
	//overriding method
	public B m1() {
		System.out.println("I am m1 of child");
		return new B();
	}
	
}