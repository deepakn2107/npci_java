package com.npci.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;

public class SortList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*List<Integer> list=new ArrayList<Integer>();
		
		list.add(2);
		list.add(4);
		list.add(3);
		list.add(6);
		list.add(9);
		list.add(5);
		list.add(212);
		
		System.out.println(list);
		
		Collections.sort(list);
		System.out.println(list);
		
		Collections.reverse(list);
		System.out.println(list);
		*/
		
		List<Employee> employees =new ArrayList<Employee>();
		
		employees.add(new Employee(0,"Deepak",22,500000));
		employees.add(new Employee(1,"Gowtham",22,500000));
		employees.add(new Employee(33,"Abijith",22,500000));
		employees.add(new Employee(23,"Devi",22,500000));
		employees.add(new Employee(2,"Nithish",22,500000));
		
		//Collections.sort(employees, new MySort());
		//ystem.out.println(employees);
		
		
		//Collections.sort(employees,(o1,02)->(int)(o2.getSalary()-o1.getSalary()));
		
		Collections.sort(null);
 	}

}

class MySort implements Comparator<Employee>{
	
	public int compare(Employee o1, Employee o2) {
		
		return(int)(o2.getId()-o1.getId());
	}
}


