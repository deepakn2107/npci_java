package com.npci.application;

public class MultipleCatchBlock {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try 
		{
			int a[]=new int[5];
			a[5]=30/0;
			
		}
		catch(ArithmeticException deep)
		{
			System.out.println("Arithmetic Exception occurs");
		}
		catch(ArrayIndexOutOfBoundsException deep)
		{
			System.out.print("ArrayIndexOutOfBound Exception occurs");
			
		}catch(Exception deep)
		{
			System.out.print("Parent Exception Occurs");
		}
		System.out.println("Rest of the Code");
	}

}
