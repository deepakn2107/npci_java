package com.npci.application;
import java.util.Scanner;

public class TestThrow {
	public static void validate(int age) {  
        if(age<18) {  
            //throw Arithmetic exception if not eligible to vote  
            throw new ArithmeticException("Person is not eligible to vote");    
        }  
        else {  
            System.out.println("Person is eligible to vote!!");  
        }  
    }  
    //main method  
    public static void main(String args[]){  
        try (//calling the function  
		Scanner sc = new Scanner(System.in)) {
        	System.out.println("Enter your age");	
        	int age=sc.nextInt();
			
				
			validate(age);
		}
    	System.out.println("rest of the code...");    
  }    
}    


