package practicePrograms;

class Department{
	int deptId;
	String name;
	Department (int id, String n){
		
			deptId = id;
			name= n;
			
	}
	
	Department(Student s){
		
		
		
	}
	void display() {
		System.out.println("Dept id : "+deptId);
		System.out.println("Dept Name : "+name);
	}

}

class Students implements Cloneable {
	int id;
	String name;
	Department dept;
	Students(int i, String n, Department d){
		id =i;
		name= n;
		dept =d;
		
		
	}
	
	public Students(Students s) {
		
		this.id = s.id;
		this.name= s.name;
	}

	void display() {
		System.out.println("Dept id : "+id);
		System.out.println("Dept Name : "+name);
		dept.display();
	}
	
	
	
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
public class CloningDemo {

	public static void main(String[] args) throws CloneNotSupportedException  {
		// TODO Auto-generated method stub
		Department dept = new Department(1, "Physics");
		Students s1 = new Students (1,"Hari",dept);
		
		Students s2=(Students) s1.clone();
		Students s3 = new Students(s1);
		dept.name="Maths";
		
//		s1.display();
//		s2.display();
		s3.display();
	}

}
