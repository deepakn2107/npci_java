package com.npci.collections;

import java.util.List;
import java.util.ArrayList;

//How to create an ArrayList using the ArrayList() Constructor
public class CreateArrayListFromCollections {
	public static void main(String[] args) {
		
		//Creating a new array list object
		List<Integer> firstFivePrimeNumbers =new ArrayList<>();
		
		firstFivePrimeNumbers.add(2);
		firstFivePrimeNumbers.add(3);
		firstFivePrimeNumbers.add(5);
		firstFivePrimeNumbers.add(7);
		firstFivePrimeNumbers.add(11);
		
		
		List<Integer> firstTenPrimeNumbers =new ArrayList<>(firstFivePrimeNumbers);
		
		
		List<Integer> nextFivePrimeNumbers =new ArrayList<>();
		
		nextFivePrimeNumbers.add(13);
		nextFivePrimeNumbers.add(17);
		nextFivePrimeNumbers.add(19);
		nextFivePrimeNumbers.add(23);
		nextFivePrimeNumbers.add(29);
		
		
		firstTenPrimeNumbers.addAll(nextFivePrimeNumbers);
		
		System.out.println(firstTenPrimeNumbers);
	}

}
