package com.npci.application;
import java.util.*;
class Book{
	int id;
	String name,author, publisher;
	int quantity;
	public Book(int id, String author, String publisher, String name, int quantity) {
		this.id=id;
		this.name=name;
		this.author=author;
		this.publisher=publisher;
		this.quantity=quantity;
		
	}

public class Hashbook {

	public void main(String[] args) {
		// TODO Auto-generated method stub
		HashSet <Book> set=new HashSet<Book>();
		Book b1=new Book(101,"Let us C","Yashwant Karetkar", "BPB",8);
		Book b2=new Book(102,"Data Communication and Networking","Forozan","MC Graw Hill",4);
		Book b3=new Book(103, "Operating System", "Galvin", "Wiley",6);
		
		set.add(b1);
		set.add(b2);
		set.add(b3);
		
		for(Book b:set) {
			System.out.println(b.id+" "+b.author+" "+b.publisher+" "+b.quantity);
		}
	}

}
}
