package com.npci.collections.linkedList;

import java.util.LinkedList;

public class CreateLinkedList {
	public static void main(String[] args) {
		LinkedList<String> fruits = new LinkedList<>();
		fruits.add("Banana");
		fruits.add("Apple");
		fruits.add("Mango");
		fruits.add("Orange");
		fruits.add("Grapes");

		System.out.println(fruits);

		// Add an element at the specified position int the LinkedLIst
		fruits.add(1, "Melon");
		fruits.addFirst("Butter Fruit");
		fruits.addLast("Choco");
		System.out.println(fruits);

	}

}
