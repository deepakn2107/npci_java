package opps;

public class Student {
	int studentId;
	String studentName;
	String studentCity;
	
	public Student() {
		
		System.out.println("Creating object ");
	}
	
	public void study() {
		System.out.println(studentName+"is Studying");
	}
	public void showFullDetails() {
		System.out.println("Student Name is "+studentName);
		System.out.println("Student Id is "+studentId);
		System.out.println("Student City is "+studentCity);
	}
}
