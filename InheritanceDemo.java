package practicePrograms;

class Employee { // parent class
	private String name;
	private double salary;

	Employee(String nam, double sal) {
		name = nam;
		salary = sal;

	}

	Employee() {
		name = "";
		salary = 0.0;
	}

	String getName() {
		return name;
	}

	void setName(String nam) {
		name = nam;
	}

	double getSalary() {
		return salary;

	}

	void setSalary(double sal) {
		salary = sal;
	}

	double raiseSalary(double percent) {
		return salary += salary * percent / 100;

	}

}

class Manager extends Employee {// derived class
	double bonus;

	public Manager(String nam, double sal, double bon) {
		super(nam, sal);
		this.bonus = bon;
	}

	double getBonus() {
		return bonus;
	}
	void setBonus(double bon) {
		bonus=bon;
	}

	public double getSalary() {// method overriding
		return super.getSalary() + bonus;
	}
}

public class InheritanceDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee e1 = new Employee("Ram", 35000);
		e1.raiseSalary(50);

		// System.out.println(e1.getSalary());

		Manager m1 = new Manager("Deepak", 60000.0, 0.0);
		//System.out.println(m1.getSalary());

		//System.out.println(m1.raiseSalary(10));

		m1.setBonus(20000);
		System.out.println(m1.getSalary());
		Employee e2=new Manager("Dinesh",45000,0);
		Manager m2=(Manager) e2;
		m2.setBonus(20000);
		
		Employee[] employees=new Employee[5];
		
		employees[0]=new Employee("Ram", 35000);
		employees[1]=new Employee("Devi", 35000);
		employees[2]=new Employee("Gowtham", 35000);
		employees[3]=m1;
		employees[4]=m2;
		
		
		for(Employee e:employees) {//dynamic binding
			
			System.out.println(e.getName()+" "+e.getSalary());
		}
	}

}
