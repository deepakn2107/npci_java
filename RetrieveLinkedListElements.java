package com.npci.collections.linkedList;

import java.util.LinkedList;

public class RetrieveLinkedListElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		LinkedList<String> fruits = new LinkedList<>();
		fruits.add("Banana");
		fruits.add("Apple");
		fruits.add("Mango");
		fruits.add("Orange");
		fruits.add("Grapes");
		
		fruits.getFirst();
		fruits.getLast();
		fruits.get(0);
		
		for (String s:fruits) 
		{
			System.out.println(s);
			
			
		}
		System.out.println(fruits.contains("Apple"));
	}

}
