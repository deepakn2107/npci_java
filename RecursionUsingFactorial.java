package StackDemo;

import java.util.Scanner;

public class RecursionUsingFactorial {

	static int factorial(int n) {
		if (n == 0)
			return 1;
		return n * factorial(n - 1);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Enter a number to Factorial");
		Scanner scan = new Scanner(System.in);
		int number = scan.nextInt();

		System.out.println("The factorial of given number is " + factorial(number));
		
	}

}
