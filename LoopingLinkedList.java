package com.npci.collections.linkedList;

import java.util.Iterator;
import java.util.LinkedList;

public class LoopingLinkedList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LinkedList<String> books = new LinkedList<>();

		books.add("Hellen Keller");
		books.add("Three men in a Boat");
		books.add("Wings of fire");
		books.add("Marvel");
		books.add("DC Kingdom");
		
		
		// int a = books.size();
		for (String str : books)
			System.out.println(str);

		for (Iterator<String> iterator = books.iterator(); iterator.hasNext();) {
			String list = (String) iterator.next();
			System.out.println(list);
		}

		int length = books.size();
		for (int i = 0; i < length; i++)
			System.out.println(books.get(i));
		
		Iterator<String> iterator = books.iterator();
		while (iterator.hasNext()) {
			String list1 = (String) iterator.next();
			System.out.println(list1);
		}

	}

}
