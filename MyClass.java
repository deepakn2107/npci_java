package opps;

abstract public class MyClass {

	public void calc() 
	{
		System.out.println("Calculating Results");
	}
	
	abstract public void launchRocket();
}
class Start{
	public static void main(String[] args) 
	{
		MyChild myChild=new MyChild();
		myChild.launchRocket();
		//myChild.calc();
	}
}
