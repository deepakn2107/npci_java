package StackDemo;

public class VarArgs {

	static double maxValues(double...ds) {
		
		double max=Double.MIN_VALUE;
		for (double k:ds) {
			if (k>max)
				max=k;
		}
		return max;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double mx=maxValues(2.3,4.5,6.4);
		System.out.println(mx);
		double mxx= maxValues(5.9,8.6,8.7,8.00,2.45);
		System.out.println(mxx);
	}

}
