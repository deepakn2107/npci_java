package com.npci.collections;

import java.util.ArrayList;
import java.util.Iterator;
//import java.util.Arrays;
import java.util.List;

public class LIstDemo {
public static void main(String[] args) {
	List <String> list = new ArrayList<>();
	
	list.add("Element1");
	list.add("Element2");
	list.add("Element3");
	list.add("Element4");
	list.add("Element5");
	
	int length =list.size();
	for(int i=0; i<length; i++)
		System.out.println(list.get(i));
	
	
	for(String string:list) {
		System.out.println(string);
	}
	//System.out.println(list.get(3));
	
	for(Iterator iterator =list.iterator();iterator.hasNext();){
		String course =(String) iterator.next();
		System.out.println(course);
	}
	
	//list.stream().forEach(list->System.out.println(list));
	
	
	
}
}
