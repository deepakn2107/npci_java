package com.npci.application;

public class TestFinallyBlock {
	public static void main(String args[])
	{
		try 
		{
			int data=25/5;
		}
		catch(NullPointerException dee)
		{
			System.out.println(2);
			
		}
		finally {
			System.out.println("Finaly block is alwys executed");
		}
		System.out.println("Rest of the Code");
	} 

}
