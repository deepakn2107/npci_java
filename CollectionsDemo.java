package com.npci.collections;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionsDemo {
	public static void main(String [] args) {

	Collection<String> fruitcollection =new ArrayList<>();
	
	fruitcollection.add("Banana");
	fruitcollection.add("Apple");
	fruitcollection.add("Grapes");
	System.out.println(fruitcollection);
	
	fruitcollection.remove("Apple");
	
	System.out.println(fruitcollection);
	System.out.println(fruitcollection.contains("Banana"));
	
	System.out.println(fruitcollection.size());
	
	fruitcollection.forEach((fruit)->{
		
	
	System.out.println(fruit);
	});
}
}
